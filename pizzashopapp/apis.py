from .serializers import PizzaShopSerializer, PizzasSerializers
from .models import PizzaShop, Pizza

from django.http import JsonResponse


def pizzashoop_api_get(request):
    serializer = PizzaShopSerializer(PizzaShop.objects.all().order_by("-id"), many=True,
                                     context={"request": request}).data

    return JsonResponse({"pizzashop": serializer})


def pizzas_api_get(request, pizzashop_id):
    pizzas = PizzasSerializers(Pizza.objects.filter(pizzashop_id=pizzashop_id).order_by("-id"), many=True,
                               context={"request": request}).data
    return JsonResponse({"pizzas": pizzas})
