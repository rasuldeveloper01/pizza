from rest_framework import serializers
from .models import PizzaShop, Pizza


class PizzaShopSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    def get_logo(self, pizzashop):
        request = self.context.get('request')
        logo_url = pizzashop.logo.url
        return request.build_absolute_uri(logo_url)

    class Meta:
        model = PizzaShop
        fields = ("id", "name", "phone", "address", "logo")


class PizzasSerializers(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    pizzashop = serializers.SerializerMethodField()

    def get_pizzashop(self, pizzashop):
        return pizzashop.name

    def get_image(self, pizzas):
        request = self.context.get("request")
        image_url = pizzas.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = Pizza
        fields = ('id', 'pizzashop', 'name', 'short_descriptions', 'image', 'price')
