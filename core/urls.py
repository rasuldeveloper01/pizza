"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from pizzashopapp.views import pizzashop_sign_up, pizzashop_account, pizzashop_pizza, home, pizzashop_home, \
    pizzashop_add_pizza, pizzashop_edit_pizza
from pizzashopapp.apis import pizzashoop_api_get, pizzas_api_get
from django.contrib.auth import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', home, name='home'),
                  path('pizzashop/sign-in/', views.LoginView.as_view(template_name="pizzashopapp/sign_in.html"),
                       name="pizzashop-sign-in"),
                  path('pizzashop/sign-out/', views.LogoutView.as_view(next_page="/"), name='pizzashop/sign-out'),
                  path('pizzashop/', pizzashop_home, name="pizzashop-home"),
                  path("pizzashop/sign-up/", pizzashop_sign_up, name="pizzashop-sign-up"),
                  path("pizzashop/account/", pizzashop_account, name="pizzashop-account"),
                  path("pizzashop/pizza/", pizzashop_pizza, name="pizzashop-pizza"),
                  path("pizzashop/add/pizza/", pizzashop_add_pizza, name='pizzashop-add-pizza'),
                  path("pizzashop/edit/pizza/<int:pizza_id>/", pizzashop_edit_pizza, name='pizzashop-edit-pizza'),

                  # API
                  path("api/client/pizzashop/", pizzashoop_api_get),
                  path("api/client/pizzas/<int:pizzashop_id>/", pizzas_api_get)

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
